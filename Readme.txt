** Created by William Rojas **

Before being able to run this in the server, two packages are needed:

1. SSIS Extensions Installer: 
	Third party software which can be installed using the provided installer --> "SSIS Extensions Installer.msi"

2. SFTPer:
	Custom dll (SFTPer.dll -- created by wrojas) to be able to upload files -- this is needed because the third party above is unable to authenticate/log into some FTPs that require "KeyboardInteractive" authentication (not sure why)
	Note that this library requires Renci.SshNet.Net4.dll, which is installed with the package above.

	To install the dll, follow these steps:
	2.1. Copy SFTPer.dll to "C:\Program Files (x86)\Microsoft SQL Server\110\DTS\Tasks"
	2.2. Register the dll:
		a. Make sure you are an administrator on the server
		b. Run powershell (as an admin)
		c. run the following commands:
			Set-location "C:\Program Files (x86)\Microsoft SQL Server\110\DTS\Tasks"
			[System.Reflection.Assembly]::Load("System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
			$publish = New-Object System.EnterpriseServices.Internal.Publish
			$publish.GacInstall("C:\Program Files (x86)\Microsoft SQL Server\110\DTS\Tasks\SFTPer.dll")

You should be able to run the SSIS packages on the server at this point.